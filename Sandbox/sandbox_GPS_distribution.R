e_verbose <- cbind(treatment, data.frame(e)) %>% setNames(c("treatment", sprintf("Q%d", 1:5)))

# Check overlap
for (i in 1:J) {
  
  g <- ggplot(e_verbose_base, aes(x = eval(parse(text = sprintf("Q%d", i))), color = treatment)) + 
    stat_density(geom = "line", position = "identity") +
    scale_color_brewer(palette = "Set2") + 
    theme(legend.position = "bottom", axis.title.y = element_blank()) +
    labs(title = sprintf("Propensity to Q%d", i), x = "Estimated GPS")
  
  assign(sprintf("g_e_%d", i), g)
  
}

mylegend<-g_legend(g_e_1)

p3 <- grid.arrange(arrangeGrob(g_e_1 + theme(legend.position="none"),
                               g_e_2 + theme(legend.position="none"),
                               g_e_3 + theme(legend.position="none"),
                               g_e_4 + theme(legend.position="none"),
                               g_e_5 + theme(legend.position="none"),
                               nrow=2, ncol = 3),
                   mylegend, nrow=2,heights=c(10, 1))

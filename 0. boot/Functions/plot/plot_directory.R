plot_directory <- function() {
  
  path <- sprintf("backend/%s",list.files(path = "0. boot", pattern = "*.R", recursive = TRUE))
  
  x <- lapply(strsplit(path, "/"), function(z) as.data.frame(t(z)))
  x <- rbind.fill(x)
  x$pathString <- apply(x, 1, function(x) paste(trimws(na.omit(x)), collapse="/"))
  
  return(data.tree::as.Node(x))
  
}
get_X_bar_p_k <- function(X_k, D, h) {
  
  num <- sum(rowSums(D*h*X_k))
  den <- sum(rowSums(D*h))
  
  return(num/den)
  
}

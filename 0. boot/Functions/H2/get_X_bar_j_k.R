get_X_bar_j_k <- function(X_k, D_j, w_j) {
  
  return(sum(D_j*X_k*w_j)/sum(D_j*w_j))
  
}

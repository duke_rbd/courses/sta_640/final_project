get_block_sum <- function(b, student, assign_block) {
  
  # Stats
  block <- student[assign_block == b, ]; treatment <- block[, lab_treatment]; outcome <- block[, lab_outcome]
  
  # Get difference in means and t-stat
  ## Prepare data
  block_w.o_out <- select(block, -one_of(lab_outcome))
  X <- model.matrix(formula(sprintf("%s ~ .", lab_treatment)), block_w.o_out)[ , -1]
  block_w.o_treat <- as.data.frame(cbind(X, outcome)) %>% setNames(c(colnames(X), lab_outcome))
  
  block_sum <- bind_rows(apply(block_w.o_treat, 2, get_X_i_sum, treatment)) %>% 
    mutate(pair = rep(combn_lab, ncol(block_w.o_treat)),
           covariate = rep(names(block_w.o_treat), each = length(combn_lab))) %>%
    select(pair, covariate, diff, se, t_score)
  
  return(block_sum)
  
}

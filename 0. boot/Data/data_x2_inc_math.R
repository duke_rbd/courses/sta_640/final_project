# Labels
lab_outcome <- "X2TXMTH"; lab_treatment <- "X2FAMINCOME"
lab_recode <- "X2STU30OCC_STEM1"
lab_factor <- c("X2SEX", "X2RACE", "X2CONTROL", "X2REGION", lab_recode)
lab_cont <- c("X1SCHOOLENG", "X1TMEFF", "X2MTHID", "X2MTHUTI", "X2MTHEFF", "X2MTHINT")
lab_covariates <- c(lab_factor, lab_cont)

# Student data set
student <- student_raw %>%
  dplyr::select(one_of(lab_outcome, lab_treatment, lab_covariates)) %>%
  filter_all(all_vars(-5 < .)) %>%
  mutate_at(vars(lab_factor), as.factor) %>% mutate_at(vars(lab_treatment), as.ordered) %>%
  mutate_at(vars(lab_recode), stem_recode)




######### Other major data sets #########


## outcome
outcome <- student[, lab_outcome]

## treatment
treatment <- student[, lab_treatment]; treatment_levels <- levels(treatment)

## data w/o outcome (for propensity score)
student_w.o_out <- student %>% dplyr::select(-one_of(lab_outcome))
X <- model.matrix(formula(sprintf("%s ~ .", lab_treatment)), student_w.o_out)[ , -1]

# Propensity score
mod <- polr(formula(sprintf("%s ~ .", lab_treatment)), student_w.o_out)
BX  <- predict(mod, student_w.o_out)


## Confusion matrix
table(treatment, BX)

## Aggregation
### Fold 1 into 2; 6 into 5
assign_block <- recode_factor(BX, `1` = 2, `6` = 5, .default = as.numeric(levels(BX)))

blockSize <- as.vector(table(assign_block)); lab_block <- levels(assign_block); num_blocks <- length(lab_block)

## n
n <- nrow(student)

## J
J <- length(treatment_levels)

## K (i.e., number of covariates)
K <- length(lab_covariates)

## Z
Z <- as.numeric(treatment)

## Pairings

combn_lvls <- matrix(c(head(1:J, -1), tail(1:J, -1)), ncol = 2)
combn_lab <- apply(combn_lvls, 1, function(pair) paste(pair, collapse = "-"))

student.dat <- student  # student.dat created for sampling

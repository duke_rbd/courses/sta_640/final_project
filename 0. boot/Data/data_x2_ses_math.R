# Labels
lab_outcome <- "X2TXMTH"; lab_treatment <- "X2SESQ5_U"
lab_recode <- "X2STU30OCC_STEM1"
lab_factor <- c("X2SEX", "X2RACE", "X2CONTROL", "X2REGION", lab_recode)
lab_cont <- c("X1SCHOOLENG", "X1TMEFF", "X2MTHID", "X2MTHUTI", "X2MTHEFF", "X2MTHINT")
lab_covariates <- c(lab_factor, lab_cont)

# Student data set
student <- student_raw %>%
  dplyr::select(one_of(lab_outcome, lab_treatment, lab_covariates)) %>%
  filter_all(all_vars(-5 < .)) %>%
  mutate_at(vars(lab_factor), as.factor) %>% mutate_at(vars(lab_treatment), as.ordered) %>%
  mutate_at(vars(lab_recode), stem_recode)

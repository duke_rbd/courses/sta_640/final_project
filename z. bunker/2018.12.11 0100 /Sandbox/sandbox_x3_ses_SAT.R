# source("0. boot/startup.R")

# Balance

## IPW
balance_metrics_IPW <- get_balance_metrics(e, J, n, treatment_levels, X, D, Z, "IPW")

## Overlap
balance_metrics_overlap <- get_balance_metrics(e, J, n, treatment_levels, X, D, Z, "overlap")

plot_balance_metrics(balance_metrics_IPW, balance_metrics_overlap)


# WACD

## Point estimates
WACD_point_est <- get_WACD(outcome, treatment, e, J, n, treatment_levels)

## Bootstrap
WACD_samples <- get_WACD_samples(100, mode = "write")
# WACD_samples <- get_WACD_samples(1000, mode = "read")

## Get data summaries
WACD_table <- get_WACD_table(WACD_point_est, WACD_samples)

write_csv(WACD_table, "0. boot/Data/WACD_table_x3_ses_SAT.csv")

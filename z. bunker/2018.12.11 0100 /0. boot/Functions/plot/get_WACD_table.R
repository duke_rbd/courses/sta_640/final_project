get_WACD_table <- function(WACD_point_est, WACD_samples) {
  
  Pctl_0.025 <- function(x) quantile(x, 0.025)
  Pctl_0.975 <- function(x) quantile(x, 0.975)
  
  CI_WACD <- WACD_samples %>% 
    group_by(method) %>% 
    summarise_all(funs(var, Pctl_0.025, Pctl_0.975))
  
  WACD <- merge(WACD_point_est, CI_WACD)
  
  for (pair in combn_lab) {
    
    table_i <- WACD %>% 
      dplyr::select(one_of("method", sprintf("%s_Pctl_0.025", pair), pair, 
                    sprintf("%s_Pctl_0.975", pair), sprintf("%s_var", pair))) %>%
      setNames(c("method", "0.025%", "point estimate", "0.975%","SE")) %>%
      mutate(SE = sqrt(SE))
    
    assign(sprintf("table_%s", match(pair, combn_lab)), table_i)
    
  }
  
  table_WACD <- rbind(table_4, table_3, table_2, table_1)
  
  kable_as_image(
  kable(table_WACD, "latex",
        caption = "Point estimate, standard error, and 95\\% CI intervals for WACD",
        booktabs = T, row.names = FALSE, digits = 2) %>%
    kable_styling(latex_options = "hold_position") %>%
    group_rows("Q5-Q4", 1, 2) %>% group_rows("Q4-Q3", 3, 4) %>% group_rows("Q3-Q2", 5, 6) %>% group_rows("Q2-Q1", 7, 8)
  , filename = "Presentation/plots/WACD_table")
  
  
  
  return(table_WACD)
  
}

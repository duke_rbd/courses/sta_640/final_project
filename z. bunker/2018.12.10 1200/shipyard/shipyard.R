
############# Cummulative min max #############

table_CI_ATE_real <- data_frame(income = head(income_brakets, -1), ATE = cum_ATE, 
                                lwr = cumsum(ATE_lwr), upr = cumsum(ATE_upr))

ggplot(table_CI_ATE, aes(x = income, y = ATE)) +
  geom_ribbon(ymin = table_CI_ATE$lwr, ymax = table_CI_ATE$upr, alpha = 0.2) +
  geom_point() + geom_line() + ylim(c(min(table_CI_ATE$lwr), max(table_CI_ATE$upr)))


############# Ordinal treatment #############

# mod <- polr(formula(sprintf("%s ~ .",lab_treatment)), student_w.o_out)
# e   <-  fitted(mod)


############# Overlap check #############

# Trim non-overlapping units
overlap_sum <- as.data.frame(cbind(treatment, e_base)) %>% group_by(treatment) %>%
  summarise_all(funs(min, max))

## Check
# apply(e_base[treatment == 1, ], 2, min)

student <- student_base; e <- e_base; e_verbose <- e_verbose_base
for (i in 1:J) {
  
  mins <- overlap_sum[, sprintf("%d_min",i)]; maxs <- overlap_sum[, sprintf("%d_max",i)]
  max_min <- max(mins); min_max <- min(maxs)
  overlap_idx <- (max_min < e[, i]) & (e[, i] < min_max)
  student <- student[overlap_idx, ]; e <- e[overlap_idx, ]; e_verbose <- e_verbose[overlap_idx, ]
  
}

g_e_1 <- ggplot(e_verbose, aes(x = Q1, color = treatment)) + 
  stat_density(geom = "line", position = "identity") +
  scale_color_brewer(palette = "Set2", labels = sprintf("Q%d", 1:5)) + 
  theme(legend.position = "bottom", axis.title.y = element_blank()) +
  labs(title = "Propensity to Q1", x = "Estimated GPS")

# Feedback
print(c(nrow(student_raw), nrow(student_base), nrow(student)))



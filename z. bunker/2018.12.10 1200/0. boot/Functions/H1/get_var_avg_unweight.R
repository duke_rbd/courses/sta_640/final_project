get_var_avg_unweight <- function(X_k, Z) {
  
  vars <- sapply(1:J, function(j) {var(X_k[Z == j])})
  return(mean(vars))
  
}

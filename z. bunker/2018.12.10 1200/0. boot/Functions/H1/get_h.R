get_h <- function(method) {
  
  if (method == "IPW") {
    
    h <- rep(1, n)
    
  } else if (method == "overlap") {
    
    h <- apply(e, 1, function(row) {1/sum(1/row)})
     
  }
  
  return(unname(h))
  
}
